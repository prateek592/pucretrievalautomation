import os
import threading
import os.path
from os import path
from selenium.common.exceptions import NoSuchElementException
import base64
import csv
from selenium import webdriver
from selenium.webdriver.support.select import Select
import requests
import json
vehicleObj = []
main_dir_path = os.getcwd()
# os.startfile(main_dir_path + "//vehCategory.csv")
# exit()
def editingvehCategory(vehicleCategory , msg , threadNo):
    # print("HERE AT EDITING VEH CATEGORY")
    csvFileName = main_dir_path + "//vehCategory.csv"
    # csvFileName = "vehCategory.csv"
    vehicleCategory = vehicleCategory.lower()
    with open(csvFileName, 'r') as f:
        reader = csv.reader(f, delimiter=',')
        lines = []
        for line in reader:
            if line[0] == str(vehicleCategory):
                line[1] = str(msg)
                line[3] = threadNo
            lines.append(line)
        f.close()
    # print("linesss->>",lines)
    # time.sleep(1000)
    with open(csvFileName, 'w', newline='') as f:
        writer = csv.writer(f, delimiter=',')
        writer.writerows(lines)
        f.close()
    # sendPUCData()

def editingCSV(vechileRegNo, msg , threadNo, tableDict , csvFileName):
    # print("HERERERERERER")
    # print(csvFileName)
    response = checking_if_csv_completed(csvFileName)
    if response == True:
        return True
    with open(csvFileName, 'r') as f:
        reader = csv.reader(f, delimiter=',')
        lines = []
        for line in reader:
            if line[0] == str(vechileRegNo):
                line[1] = str(msg)
                line[3] = threadNo
                line[5] = json.dumps(tableDict)
            lines.append(line)
        f.close()
    with open(csvFileName, 'w', newline='') as f:
        writer = csv.writer(f, delimiter=',')
        writer.writerows(lines)
        f.close()

def vehicleAllocation(threadNo , vehicleObj):
    try:
        for i in range(len(vehicleObj)):
            vehicleRegNo = vehicleObj[i]['vechRegno']
            if vehicleObj[i]['isTaken'] == False:
                vehicleObj[i]['isTaken'] = True
                vehicleObj[i]['threadNo'] = threadNo
                return vehicleRegNo
        return None
    except Exception as e:
        print(e)
def vehicleAllocationCSV(threadNo):
    try:
        for i in range(len(vehicleObj)):
            vechFirst6 = vehicleObj[i]['vechFirst6']
            if vehicleObj[i]['isTaken'] == False:
                vehicleObj[i]['isTaken'] = True
                vehicleObj[i]['threadNo'] = threadNo
                return vechFirst6
        return None
    except Exception as e:
        print(e)

def check_exists_by_id_puc(driver , id):
    try:
        driver.find_element_by_id(id)
    except NoSuchElementException:
        return False
    return True


def check_exists_by_class_puc(driver , clss):
    try:
        driver.find_element_by_class_name(clss)
    except NoSuchElementException:
        return False
    return True

def checking_if_csv_completed(csvFileName):
    # print("in checking if empty")
    vehicleCSVDict = []
    with open(csvFileName, 'r') as f:
        reader = csv.reader(f)
        for line in reader:
            if line[1] == '':
                vehicleCSVDict.append(line[0])
    # print(" Checking Vehicle Dict",vehicleCSVDict)
    # veh = str(vehicleCSVDict)
    # print(veh[:30])
    # print("length is-->",len(vehicleCSVDict))
    if len(vehicleCSVDict) < 1:
        return True

def createCSVDictAllocation(csvFileName):
    vehicleCSVDict = []
    # print("CSV File Name",csvFileName)
    with open(csvFileName, 'r') as f:
        reader = csv.reader(f)
        for line in reader:
            if line[1] == '':
                vehicleDict = {
                    "vechRegno": line[0],
                    "msg": None,
                    "isTaken" : False,
                    "threadNo": None,
                    "upload": None,
                    "data": None
                }
                vehicleCSVDict.append(vehicleDict)
    # print("Vehicle Dict",vehicleCSVDict)
    # veh = str(vehicleCSVDict)
    # print(veh[:30])
    if len(vehicleCSVDict) == 0:
        return None
    return vehicleCSVDict

def apiCall(url, data, headers):
    try:
        r = requests.post(url=url, json=data, headers=headers)
        return r
    except Exception as e:
        print(e)

def editVehCategory(vehFirst6):
    # print("in editing file")
    # csvFileName = "vehCategory.csv"
    csvFileName = main_dir_path + "//vehCategory.csv"
    # print("veh first 6 ", vehFirst6)
    vehicleCategory = vehFirst6.lower()
    # print("veh categdshbhdsbhbfhsdf->>",vehicleCategory)
    with open(csvFileName, 'r') as f:
        # print("opening veh csv")
        reader = csv.reader(f, delimiter=',')
        lines = []
        for line in reader:
            if line[0] == str(vehicleCategory):
                # print("UNDER IFF")
                line[4] = "Yes"
            lines.append(line)
        f.close()
    # print(lines)
    with open(csvFileName, 'w', newline='') as f:
        writer = csv.writer(f, delimiter=',')
        writer.writerows(lines)
        f.close()

def dataToBase64(file):
    file = main_dir_path + "\\puc\\" + file
    fileName = str(file)
    # print("befire sending file",file)
    # print("HERE SENDING DATA TO BASE64")
    with open(file, encoding='utf8') as f:
        csv_string = f.read().strip()
    encoded = base64.b64encode(csv_string.encode('utf-8'))
    apihitter = "data:application/csv;base64," + encoded.decode("utf-8")
    # print(apihitter)
    # print(apihitter[:60])
    with open("base64.txt" , "w") as file:
        file.write(apihitter)
    apiURL = "http://localhost/booster_webservices/Automation/sendPUCInfo"
    # print("Api Length For User {userid} is {apilength} ".format(userid=id, apilength=len(apihitter)))
    # data to be sent to api
    # print(userid)
    data = {'csv': apihitter}
    headers = {'Content-Type': 'application/json'}
    # print(apiURL)
    result = apiCall(apiURL, data, headers)
    json_data = json.loads(result.text)
    print("apiResponse-->", result.text)
    try:
        if json_data["success"] == bool(1):
            # print("in sucesss")
            # print("file name",file)
            vehFirst6=fileName.split('.')
            # print("file name", vehFirst6)
            vehFirst6 = vehFirst6[0]
            vehFirst6 = str(vehFirst6)
            # print("under if",vehFirst6)
            editVehCategory(vehFirst6)
    except Exception as e:
        pass

def sendPUCData():
    # print("HERE SENDING DATA")
    # csv_name = "vehCategory.csv"
    csv_name = main_dir_path + "//vehCategory.csv"
    with open(csv_name, 'r') as f:
        reader = csv.reader(f, delimiter=',')
        for line in reader:
            if line[1] == 'This CSV is Completed' and line[4] == '':
                fileName = str(line[0].upper()) + ".csv"
                dataToBase64(fileName)
                break
        f.close()

def createCSV(threadNo , range_of_veh):
    vehicleFirst6 = vehicleAllocationCSV(threadNo)
    print(main_dir_path + "\\" + str(vehicleFirst6) + ".csv")
    # os.startfile(main_dir_path + "\\" + str(vehicleFirst6) + ".csv")
    while (vehicleFirst6 != None):
        fileExists = path.exists(main_dir_path + "\\puc\\" +  str(vehicleFirst6) + ".csv")
        print("File Exists-->",fileExists)
        if fileExists == False:
            input = str(vehicleFirst6)
            append = 0
            vehArray = []
            while append != range_of_veh:
                if len(str(append)) <= 4:
                    zeroAppend = 4 - len(str(append))
                    zeros = ''
                    for i in range(zeroAppend):
                        zeros = zeros + "0"
                    last4digit = zeros + str(append)
                    newVehNo = input + last4digit
                    # print(newVehNo)
                    vehArray.append(newVehNo)
                append = append + 1
                # print("append-->",append)
            # print(vehArray)
            # print(len(vehArray))
            mainArray = []
            for x in vehArray:
                row = [x,'','','','','']
                mainArray.append(row)
            with open(main_dir_path + "\\puc\\" + input+'.csv', 'w') as csv_file:
                wtr = csv.writer(csv_file, delimiter=',', lineterminator='\n')
                fieldNames = ['vehRegNo','msg','isTaken','threadNo','upload','data']
                wtr.writerow(fieldNames)
                wtr.writerows(mainArray)
                csv_file.close()

            # print(mainArray)
            t = []
            # time.sleep(100)
            response = vehPucData(threadNo,main_dir_path + "\\puc\\" +  input+'.csv')
            if response == None:
                # print("HERE EDITING")
                editingvehCategory(vehicleFirst6,"This CSV is Completed", threadNo)
                sendPUCData()
            vehicleFirst6 = vehicleAllocationCSV(threadNo)
        else:
            input = str(vehicleFirst6)
            response = vehPucData(threadNo, main_dir_path + "\\puc\\" +  input + '.csv')
            if response == None:
                # print("HERE EDITING")
                editingvehCategory(vehicleFirst6,"This CSV is Completed", threadNo)
                sendPUCData()
            vehicleFirst6 = vehicleAllocationCSV(threadNo)


def vehPucData(threadNo,csvFileName):
    print("Starting For Vehicle Category -->>>",csvFileName)
    vehObject = createCSVDictAllocation(csvFileName)
    if vehObject == None:
        # print("None")
        return None
    ex_path = os.getcwd() + "\\chromedriver.exe"
    # print(ex_path)
    os.environ["webdriver.chrome.driver"] = ex_path
    options = webdriver.ChromeOptions()
    download_path = os.getcwd() + "\\poc"
    prefs = {'download.default_directory': download_path}
    options.add_experimental_option("prefs", prefs)
    options.add_argument('--ignore-certificate-errors')
    options.add_argument("--test-type")
    options.add_experimental_option("prefs", prefs)
    options.add_argument('window-size=1200x600')
    options.add_argument("--window-size=1920,1080")
    options.add_argument("--disable-gpu")
    options.add_argument("--disable-extensions")
    options.add_argument("useAutomationExtension")
    options.add_argument("--proxy-server='direct://'")
    options.add_argument("--proxy-bypass-list=*")
    options.add_argument("--start-maximized")
    options.add_argument("--headless")
    options.add_argument("--log-level=3")
    driver = webdriver.Chrome(options=options)
    URL = "https://rajasthanpuc.in/ValidationRegno.aspx"
    driver.get(URL)
    driver.maximize_window()
    vehicleRegNo = vehicleAllocation(threadNo , vehObject)
    while (vehicleRegNo != None):
        driver.find_element_by_xpath('//*[@id="HyperLink2"]').click()
        select = Select(driver.find_element_by_id('Veh_Type'))
        select.select_by_visible_text('Diesel Vehicle')
        driver.find_element_by_id('Sreg').send_keys(vehicleRegNo)
        driver.find_element_by_id('Button1').click()
        # mainPageResponse = check_exists_by_class_puc(driver, 'logo_text')
        # print(mainPageResponse)
        response = check_exists_by_id_puc(driver , 'GridView2')
        response1 = check_exists_by_id_puc(driver , 'GridView1')
        # print("response->", response)
        if response == True or response1 == True:
            row_count = len(driver.find_elements_by_xpath('//*[@id="GridView2"]/tbody/tr'))
            # print(row_count)
            row_count = str(row_count)
            # print("row count",row_count)
            lastRow = driver.find_element_by_xpath('//*[@id="GridView2"]/tbody/tr[' + row_count + ']/td[1]/a').click()
            driver.switch_to.window(driver.window_handles[1])
            tableDict = {
                "Pucc No" : driver.find_element_by_id("Pucc_lbl").get_attribute('innerText'),
                "Vehicle No ":driver.find_element_by_id("Vechno_lbl").get_attribute('innerText') ,
                "Customer Name":driver.find_element_by_id("customername_lbl").get_attribute('innerText') ,
                "Customer Mobile": driver.find_element_by_id("Cmobile_lbl").get_attribute('innerText'),
                "Year of Regn": driver.find_element_by_id("Regyear_lbl").get_attribute('innerText'),
                "Type of Vehicle": driver.find_element_by_id("vehtype_lbl").get_attribute('innerText'),
                "Make" : driver.find_element_by_id("Make_lbl").get_attribute('innerText'),
                "Model": driver.find_element_by_id("Model_lbl").get_attribute('innerText'),
                "Test Date": driver.find_element_by_id("TestDate_lbl").get_attribute('innerText'),
                "Time": driver.find_element_by_id("TestTime_lbl").get_attribute('innerText'),
                "Valid UpTo": driver.find_element_by_id("validdate_lbl").get_attribute('innerText'),
                "Center Name": driver.find_element_by_id("centername_lbl").get_attribute('innerText'),
                "Center Address": driver.find_element_by_id("centeraddress_lbl").get_attribute('innerText'),
                "Licence No": driver.find_element_by_id("licence_lbl").get_attribute('innerText'),
                "Test Result": driver.find_element_by_id("txtResult").get_attribute('innerText')
            }
            # print(tableDict)
            print("Data Grabbed For Vehicle Number-->{} and Data is {}".format(vehicleRegNo , tableDict))
            response = editingCSV(vehicleRegNo , "Success For This Veh No", threadNo , tableDict , csvFileName)
            if response == True:
                # print("HERE QUITTING DRIVER")
                driver.quit()
                return
            vehicleRegNo = vehicleAllocation(threadNo , vehObject)
            tableDict = {}
            driver.close()
            driver.switch_to.window(driver.window_handles[0])
            URL = "https://rajasthanpuc.in/ValidationRegno.aspx"
            driver.get(URL)
        else:
            URL = "https://rajasthanpuc.in/ValidationRegno.aspx"
            driver.get(URL)
            print("No PUC For Vehicle No.-->{}".format(vehicleRegNo))
            response= editingCSV(vehicleRegNo, "No PUC For This Veh No", threadNo, '' ,csvFileName)
            if response == True:
                driver.quit()
            vehicleRegNo = vehicleAllocation(threadNo , vehObject)
    driver.quit()
    # sendPUCData()



if __name__ == "__main__":
    with open('vehCategory.csv', 'r') as f:
        reader = csv.reader(f, delimiter=',')
        for line in reader:
            if line[1] == '' or line[4] == '':
                vehicleDict = {
                    "vechFirst6": line[0].upper(),
                    "msg": None,
                    "index" : None,
                    "threadNo": None,
                    "isTaken" : False
                }
                vehicleObj.append(vehicleDict)
        f.close()
    # print(vehicleObj)
    f = open(main_dir_path + "//" + "threadNo.txt", "r")
    txt_thread_and_range = f.read()
    splitArrayData = txt_thread_and_range.split(",")
    no_of_thread = int(splitArrayData[0])
    range_of_veh = int(splitArrayData[1])
    # print(no_of_thread)
    # print(range_of_veh)
    puc_folder_exists = path.exists(main_dir_path + "\\puc")
    if puc_folder_exists != True:
        os.mkdir(main_dir_path + "\\puc")
    t = []
    for i in range(no_of_thread):
        t.append(threading.Thread(target=createCSV, args=(str(i + 1) , range_of_veh)))
        t[len(t) - 1].start()

