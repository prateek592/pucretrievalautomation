#!/usr/bin/env python3
import sys
import os
import logging
import requests
from datetime import datetime
import base64
import json

try:
    poc_path = r"E:\POCRetrieval"
    sys.path.append(poc_path)
    os.chdir(poc_path)
    from PUCRetrieval import POCRetrieval
    c = POCRetrieval()
    c.PUCRetrieval()
    os.chdir("..")
except Exception as e:
    print(e)
    os.chdir("..")
    pass