valid_swaps = [(0, 1), (1, 2), (3, 4), (4, 5), (6, 7), (7, 8)] + [(0, 3), (3, 6), (1, 4), (4, 7), (2, 5), (5, 8)]
primes = {2, 3, 5, 7, 11, 13, 17}

initial_permutation = (1, 2, 3, 4, 5, 6, 7, 8, 9)
list_of_permutations = [initial_permutation]
dic_of_permutations = {initial_permutation: 0}


def swap(permutation, a, b):
    temp = list(permutation)
    temp[a], temp[b] = temp[b], temp[a]
    return tuple(temp)


for permutation in list_of_permutations:
    for a, b in valid_swaps:
        if permutation[a] + permutation[b] in primes:
            new_permutation = swap(permutation, a, b)

            if new_permutation not in dic_of_permutations:
                list_of_permutations.append(new_permutation)
                dic_of_permutations[new_permutation] = dic_of_permutations[permutation] + 1

t = int(input())

for _ in range(t):
    input()
    M = tuple([int(m) for m in ' '.join([input().strip() for i in range(3)]).split()])
    # print(dic_of_permutations)
    # print(M)
    print(-1 if M not in dic_of_permutations else dic_of_permutations[M])