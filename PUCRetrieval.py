import sys
import os
import logging
from selenium.webdriver.remote.remote_connection import LOGGER
from selenium.webdriver.support.ui import Select
from PIL import Image
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
from bs4 import BeautifulSoup
import pytesseract
from selenium.common.exceptions import NoSuchElementException
import base64
import csv
import pandas as pd
from os import listdir
from io import BytesIO
import argparse
from skimage.morphology import opening
from skimage.morphology import disk
# Data handling
import numpy as np
import cv2
import urllib.request as urllib2
import lxml.html
from datetime import datetime
from selenium.webdriver import ActionChains
from selenium import webdriver
from selenium.webdriver.support.select import Select
import requests
import json
from PUC import puc
import twilio
from twilio.rest import Client

getVehicleRegNoURL = 'Test/getVehicleRegNo'
with open('uploadurl.txt', "r+") as f:
    uploadURL = f.read()

class POCRetrieval(puc):
    def __init__(self):
        self.spr = super(POCRetrieval, self)

        try:
            ex_path = os.getcwd()+"\\chromedriver.exe"
            print(ex_path)
            os.environ["webdriver.chrome.driver"] = ex_path
            #Create Chrome web driver
            chrome_options = webdriver.ChromeOptions()
            download_path = os.getcwd()+"\\poc"
            prefs = {'download.default_directory':download_path}
            chrome_options.add_experimental_option("prefs",prefs)
            chrome_options.add_argument('--ignore-certificate-errors')
            chrome_options.add_argument("--test-type")
            chrome_options.add_experimental_option("prefs",prefs)
            chrome_options.add_argument('window-size=1200x600')
            chrome_options.add_argument("--window-size=1920,1080")
            chrome_options.add_argument("--disable-gpu")
            chrome_options.add_argument("--disable-extensions")
            chrome_options.add_argument("useAutomationExtension")
            chrome_options.add_argument("--proxy-server='direct://'")
            chrome_options.add_argument("--proxy-bypass-list=*")
            chrome_options.add_argument("--start-maximized")
            chrome_options.add_argument("--headless")
            chrome_options.add_argument("--log-level=3")
            LOGGER.setLevel(logging.WARNING)
            self.driver = webdriver.Chrome(chrome_options = chrome_options)
        except Exception as e:
            print(e)
            #sys.exit()
        self.spr.__init__("1", self.driver)

    def check_exists_by_id_puc(self, id):
        try:
            self.driver.find_element_by_id(id)
        except NoSuchElementException:
            return False
        return True

    def check_exists_by_class_puc(self, classs):
        try:
            self.driver.find_element_by_id(classs)
        except NoSuchElementException:
            return False
        return True

    def PUCRetrieval(self):
        try:
            print(os.getcwd())
            print(__file__)
            print("================= POC RETRIEVAL STARTING ====================")
            r = requests.get(
                url=uploadURL + 'Test/getVehicleRegNo'
            )
            print('Hitting -> ' + uploadURL + 'Test/getVehicleRegNo')
            # print(r.text)
            json_data = json.loads(r.text)
            # print("json data-->",json_data)
            URL = "https://rajasthanpuc.in/ValidationRegno.aspx"
            self.driver.get(URL)
            print("length-->",len(json_data['data']))
            for i in range(len(json_data['data'])):
                # print("i---------->",i)
                self.driver.delete_all_cookies()
                regno = json_data['data'][i]['regno']
                doc_type = json_data['data'][i]['doc_type']
                doc_type_id = json_data['data'][i]['doc_type_id']
                entry_by = json_data['data'][i]['entry_by']
                vid = json_data['data'][i]['vid']
                # print(regno)
                # print(doc_type)
                # print(doc_type_id)
                # print(entry_by)
                # print(vid)
                print("Starting For RegNo -->{}".format(regno))

                self.spr.checkPresenceOfElement("xPath", '//*[@id="HyperLink2"]').click()
                folder_path = os.getcwd() + "\\poc\\"
                for file_name in listdir(folder_path):
                    if file_name.endswith('.png'):
                        os.remove(folder_path + file_name)
                select = Select(self.driver.find_element_by_id('Veh_Type'))
                select.select_by_visible_text('Diesel Vehicle')
                self.spr.checkPresenceOfElement('id','Sreg').send_keys(regno)
                self.spr.checkPresenceOfElement('id','Button1').click()
                self.spr.timeSleep(4)
                response = self.check_exists_by_id_puc('GridView2')
                response1 = self.check_exists_by_id_puc('GridView1')
                print("response->",response)
                if response == True or response1 == True:
                    row_count = len(self.driver.find_elements_by_xpath('//*[@id="GridView2"]/tbody/tr'))
                    # print(row_count)
                    row_count = str(row_count)
                    # print("roe count",row_count)
                    lastRow = self.driver.find_element_by_xpath('//*[@id="GridView2"]/tbody/tr['+row_count+']/td[1]/a').click()
                    self.driver.switch_to.window(self.driver.window_handles[1])
                    self.driver.maximize_window()
                    self.spr.timeSleep(3)
                    response = self.driver.execute_script("document.getElementById('form1').style.zoom = '80%';")
                    # print("response-->",response)
                    self.spr.timeSleep(3)
                    element = self.driver.find_element_by_id('form1')  # findpart of the page you want image of
                    location = element.location
                    print(location)
                    size = element.size
                    png = self.driver.get_screenshot_as_png()  # saves screenshot of entire page
                    im = Image.open(BytesIO(png))  # uses PIL library to open image in memory
                    left = location['x']
                    top = location['y']
                    right = location['x'] + size['width']
                    bottom = location['y'] + size['height']
                    im = im.crop((left, top, right, bottom))  # defines crop points
                    imageSavePath = os.getcwd() + "\\poc\\" + "poc.png"
                    im.save(imageSavePath)
                    with open(imageSavePath, "rb") as image_file:
                        encoded_string = base64.b64encode(image_file.read())


                    # print(encoded_string)
                    # print(encoded_string.decode('utf-8'))
                    apihitter = "data:image/jpeg;base64," + encoded_string.decode("utf-8")
                    url = 'http://13.126.215.102/booster_webservices/Vehicles/addVehicleDocumentWeb'
                    data = {
                        'x_base64img': apihitter,
                        'x_document_type' : doc_type,
                        'x_document_type_id':doc_type_id,
                        'x_entryby': entry_by,
                        'x_vehicle_id' : vid
                    }
                    # print("data-->",data)
                    headers = {'Content-Type': 'application/json',
                               'entrymode':'1',
                               'version':'1.0',
                               'authkey':'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6OTIsIm5hbWUiOiJBdXRvIiwibW9iaWxlbm8iOjk0NjExNjI1OTgsImVtYWlsIjpudWxsLCJ0aW1lIjoiMjAxOS0wOC0wMlQxMzoxMDoyMCswNTozMCJ9.WSkGCsd7a8LqoMW9Uo4hV1u8V9HveZdWEp8_sp3-g8U'}
                    r = self.apiCall(url, data, headers)
                    print(r.text)
                    json_data1 = json.loads(r.text)
                    self.spr.timeSleep(2)
                    self.driver.close()
                    self.driver.switch_to.window(self.driver.window_handles[0])
                    URL = "https://rajasthanpuc.in/ValidationRegno.aspx"
                    self.driver.get(URL)
                else:
                    URL = "https://rajasthanpuc.in/ValidationRegno.aspx"
                    self.driver.get(URL)
                    print("No PUC For Vehicle No.-->{}".format(regno))

            self.driver.quit()

        except Exception as e:
            print(e)
            self.spr.add_to_log('error', e)
            # self.spr.failed("Please Check Error Log With Its TimeStamp! :) " , userid)

